package routers

import (
	"books-server/controllers"

	"github.com/astaxie/beego"
)

func init() {
	beego.Router("comment", &controllers.CommentController{})
}
