package controllers

import (
	"books-server/models"
	"encoding/json"
	"github.com/astaxie/beego/orm"

	"github.com/astaxie/beego"
)

type CommentController struct {
	beego.Controller
}

func (c *CommentController) Post() {
	var comment models.Comments
	err := json.Unmarshal(c.Ctx.Input.RequestBody, &comment)
	if err != nil {
		return
	}
	rToken := true
	if comment.Token != "" {
		rToken = false
	}
	success := models.AddComment(&comment)
	ret := make(map[string]interface{}, 2)
	ret["success"] = success
	ret["token"] = ""
	if rToken {
		ret["token"] = comment.Token
	}
	c.Data["json"] = ret
	c.ServeJSON()
}

func (c *CommentController) Get() {
	cs := make([]*models.Comments, 0, 32)
	o := orm.NewOrm()
	_, err := o.QueryTable("comments").All(&cs)
	if err != nil {
		return
	}
	c.Data["json"] = cs
	c.ServeJSON()
}
