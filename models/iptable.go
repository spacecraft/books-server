package models

import "time"

var (
	allowTable  map[string]int64
	rejectTable map[string]int64
)

func init() {
	allowTable = make(map[string]int64)
	rejectTable = make(map[string]int64)
}

func CheckIp(ip string) bool {
	removeExpiredIps()
	if isRejected(ip) {
		return false
	}

	if _, ok := allowTable[ip]; ok {
		delete(allowTable, ip)
		rejectTable[ip] = time.Now().Unix()
		return true
	}
	if len(allowTable) >= 10 {
		return false
	}
	allowTable[ip] = time.Now().Unix()
	return true
}

func removeExpiredIps() {
	ts := time.Now().Unix()
	ts2 := ts - 10 * 60
	for k, v := range allowTable {
		if v <= ts2 {
			delete(allowTable, k)
		}
	}
}

func isRejected(ip string) bool {
	if _, ok := rejectTable[ip]; !ok {
		return false
	} else {
		ts := time.Now().Unix()
		ts2 := ts - 30 * 60
		if rejectTable[ip] <= ts2 {
			delete(rejectTable, ip)
			return false
		}
		return true
	}
}
