package models

import (
	"github.com/astaxie/beego/logs"
	"github.com/astaxie/beego/orm"
	"strconv"
	"time"
)

type Comments struct {
	Id         int
	Title      string
	Content    string
	Ip         string
	CreateTime time.Time
	Name       string
	Token      string
}

func AddComment(c *Comments) bool {
	if c.Token == "" {
		c.Token = strconv.FormatInt(time.Now().UnixNano() % 1e9, 10)
	}
	o := orm.NewOrm()
	c.CreateTime = time.Now()
	_, err := o.Insert(c)
	if err != nil {
		logs.Error("db error: %s", err)
		return false
	}
	return true
}
