package main

import (
	"books-server/models"
	_ "books-server/routers"
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/logs"
	"github.com/astaxie/beego/orm"
	_ "github.com/mattn/go-sqlite3"
)

func main() {
	logs.SetLogger(logs.AdapterFile, `{"filename": "test.log", "level": 7}`)

	orm.RegisterDriver("sqlite3", orm.DRSqlite)
	orm.RegisterDataBase("default", "sqlite3", "books.db")
	orm.RegisterModel(new(models.Comments))
	orm.RunSyncdb("default", true, true)
	beego.Run()
}
